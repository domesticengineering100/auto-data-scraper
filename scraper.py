from bs4 import BeautifulSoup
from urllib.request import urlopen
import re
import pandas as pd
import gc
from pathlib import Path
import numpy as np

def pull_data(vehicle):
    # print('\n\n\n')
    # print(vehicle)
    mi = vehicle.find('div', {"class": "mileage"})
    mi = int(re.sub("[^0-9]", "", mi.text))

    pr = vehicle.find('span', {'class': 'primary-price'})
    pr = int(re.sub("[^0-9]", "", pr.text))

    name = vehicle.find('h2', {"class": "title"}).text
    year = int(re.findall('\d{4}', name)[0])

    return {'miles': mi, 
            'price': pr,
            'name': name,
            'year': year}


def largest_page_number(soup):
    def convert(x):
        try:
            return int(x)
        except:
            return 0
    navigation = soup.find("nav", {"id": "pagination-navigation"})
    return np.max([convert(i.text) for i in navigation.find_all('a')])


def url_to_df(url):
    page = urlopen(url)
    html = page.read().decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")
    details = soup.find_all("div", {"class": "vehicle-details"})

    # data = [pull_data(i) for i in details]
    data = []
    for i in details:
        try: 
            data.append(pull_data(i))
        except AttributeError:
            pass
        except ValueError:
            pass
        
    return pd.DataFrame(data=data), largest_page_number(soup)


def multipage(url, pages=[1,2,3]):
    dfs = []
    
    for i in pages:
        nurl = url + f'&page={i}'
        print(nurl)
        
        out, highval = url_to_df(nurl)
        dfs.append(out)
        print(len(out))
        print(highval, i)
        if highval <= i:
            break
    df = pd.concat(dfs)
    return df


def saver(df, path: Path):
    if path.exists():
        raise ValueError('save path already exists')
    else: 
        df.to_csv(path)


def full_finder(url, pages=[1,2,3]):
    url = url.replace('&page=1', '')

    filename = re.findall(r'models\[\]=([^&]+)', url)[0]
    filename = f'data/{filename}.csv'
    print('\n')
    print(filename)
    df = multipage(url, pages=pages)
    # print(df)
    saver(df, Path(filename))
    return df 




urls = ['https://www.cars.com/shopping/results/?dealer_id=&keyword=&list_price_max=&list_price_min=&makes[]=toyota&maximum_distance=all&mileage_max=&models[]=toyota-corolla&monthly_payment=&page_size=100&sort=best_match_desc&stock_type=used&year_max=&year_min=&zip=77584',
'https://www.cars.com/shopping/results/?makes[]=toyota&maximum_distance=all&models[]=toyota-corolla&page=1&page_size=100&stock_type=used&zip=77584'
]
for url in urls:
    full_finder(url, pages=np.arange(0, 100)+1)




gc.collect()