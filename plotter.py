import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import pandas as pd
import numpy as np
from pathlib import Path
import matplotlib
import re
import gc
font = {'size'   : 18}

matplotlib.rc('font', **font)

def get_data(path: Path):
    df = pd.read_csv(path)
    if df['price'].mean() < 1000:
        df['price'] *= 1000
    if df['miles'].mean() < 1000:
        df['miles'] *= 1000
    return df

def add_amort(df, totalmiles=150000, addmiles=0):
    if addmiles == 0:
        df['amort'] = df['price']/(totalmiles - df['miles'])
    else:
        df['amort'] = df['price']/(addmiles)

def add_age(df):
    df['age'] = 2024 - df['year']

def add_trim(df):
    def mapper(x):
        m = re.search(r'\d{4} \S* \S* (\S*)', x)
        return m.group(1)
    
    df['trim'] = df['name'].map(mapper)

def add_trim_ford(df):
    def mapper(x):
        m = re.search(r'\d{4} \S* (\S*) (\S*)', x)
        return m.group(1)
    
    df['trim'] = df['name'].map(mapper)

def bestfit_lines(x, y, breakpoints):
    x = np.array(x)
    y = np.array(y)
    coeffs = []
    lines = []
    breakpoints = [np.min(x)] + breakpoints + [np.max(x)]
    for st, end in zip(breakpoints, breakpoints[1:]):
        msk = (x>=st) * (x<=end)
        if np.sum(msk) >1:
            slope, inter = np.polyfit(x[msk], y[msk], 1)
            coeffs.append([slope, inter])
            lines.append({'x': x[msk],
                          'y': slope*x[msk]+ inter})
        else:
            coeffs.append(np.array([np.nan, np.nan]))
            lines.append(None)
    return coeffs, lines

def manual_data_clean(df, maxmiles=160000, maxage=15, maxprice=90000):
    # df.drop(df.loc[(df['miles']>70000) & (df['price']>60000)].index, inplace=True)
    df.drop(df.loc[(df['miles']>maxmiles) | (df['age'] > maxage) | (df['price'] > maxprice)].index, inplace=True)

def plot_depreciation(df, SEPARATE_TRIM=False, break_years=[], break_miles=[], model_count=-1):
    print(f'Total sample size: {len(df)} vehicles')

    yearcoeffs, yearlines = bestfit_lines(df['age'], df['price'], break_years)
    milescoeffs, mileslines = bestfit_lines(df['miles'], df['price'], break_miles)

    fig, axes = plt.subplots(1, 2, sharey=True, figsize=(30,12))
    fig.suptitle(filep)
    ax = axes[0]
    ax.spines[['right', 'top']].set_visible(False)

    if SEPARATE_TRIM is True:
        for t in list(df.trim.value_counts().index[:model_count]):
            dff = df.loc[df['trim'] == t]
            ax.plot(dff['age'], dff['price'], 'o', label=t)
        ax.legend(loc='upper right')
    else:
        ax.plot(df['age'], df['price'], 'o')

    for dt, co in zip(yearlines, yearcoeffs):
        ax.plot(dt['x'], dt['y'],color='xkcd:red')
        ax.text((dt['x'].max()+dt['x'].min())/2, dt['y'].max(), f'{co[0]:.0f} $/year')
    ax.set_xlabel('Vehicle age (years)')
    ax.set_ylabel('Price ($)')
    
    print('Age coeffs ($/year)')
    for i in yearcoeffs:
        print(i[0])

    ax = axes[1]
    ax.spines[['right', 'top']].set_visible(False)
    if SEPARATE_TRIM is True:
        for t in list(df.trim.value_counts().index[:model_count]):
            dff = df.loc[df['trim'] == t]
            ax.plot(dff['miles'], dff['price'], 'o', label=t)
    else:
        ax.plot(df['miles'], df['price'], 'o')

    for dt, co in zip(mileslines, milescoeffs):
        ax.plot(dt['x'], dt['y'], color='xkcd:red')
        ax.text((dt['x'].max()+dt['x'].min())/2, dt['y'].max(), f'{co[0]:.2f} $/mi')
    ax.set_xlabel('Mileage (miles)')
    ax.set_ylim(bottom=0)
    print('\nMileage coeffs ($/mi)')
    for i in milescoeffs:
        print(i[0])
    formatter = ticker.StrMethodFormatter('${x:,.0f}')
    ax.yaxis.set_major_formatter(formatter)





T, F = True, False

SEPARATE_TRIM = F


if F:  # ford f150 plot
    filep = 'data/ford-f_150.csv'
    df = get_data(Path(filep))
    add_amort(df) 
    add_age(df)
    add_trim_ford(df)
    manual_data_clean(df, maxprice=90000, maxmiles=160000, maxage=15)
    plot_depreciation(df, SEPARATE_TRIM, [4.5], [40000], model_count=7)

if F:  # honda civic
    filep = 'data/honda-civic.csv'
    df = get_data(Path(filep))
    add_amort(df) 
    add_age(df)
    add_trim(df)
    manual_data_clean(df, maxprice=90000, maxmiles=160000, maxage=15)
    plot_depreciation(df, SEPARATE_TRIM, [], [], model_count=7)

if F:
    filep = 'data/chevrolet-silverado.csv'
    df = get_data(Path(filep))
    add_amort(df) 
    add_age(df)
    add_trim(df)
    manual_data_clean(df, maxprice=900000, maxmiles=160000, maxage=15)
    plot_depreciation(df, True, [4], [40000], model_count=7)

if F:
    filep = 'data/bmw_7series.csv'
    df = get_data(Path(filep))
    add_amort(df) 
    add_age(df)
    add_trim(df)
    manual_data_clean(df, maxprice=10**9, maxmiles=160000, maxage=15)
    plot_depreciation(df, False, [2.5, 6.5], [20000, 100000], model_count=7)

if T:
    filep = 'data/tesla-model_3.csv'
    df = get_data(Path(filep))
    add_amort(df) 
    add_age(df)
    add_trim(df)
    manual_data_clean(df, maxprice=150000, maxmiles=160000, maxage=15)
    plot_depreciation(df, False, [4], [40000], model_count=7)


gc.collect()
plt.show()
